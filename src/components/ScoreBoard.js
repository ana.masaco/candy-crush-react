import "./ScoreBoard.css";

const ScoreBoard = ({ score }) => {
  return (
    <div className="scoreBoard">
      <h2>Score: {score}</h2>
    </div>
  )
}

export default ScoreBoard