import React, {useEffect, useState} from 'react';
import ScoreBoard from './components/ScoreBoard';
import blueCandy from './images/blue-candy.png';
import greenCandy from './images/green-candy.png';
import orangeCandy from './images/orange-candy.png';
import purpleCandy from './images/purple-candy.png';
import redCandy from './images/red-candy.png';
import yellowCandy from './images/yellow-candy.png';
import blank from './images/blank.png';
import './App.css';

// this is my game's grid width
const width = 8;

// candy colors
const candyColors = [
  blueCandy,
  orangeCandy,
  purpleCandy,
  redCandy,
  yellowCandy,
  greenCandy
]


const App = () => {

  const [currentColorArrangement, setCurrentColorArrangement] = useState([]);
  const [squareBeingDragged, setSquareBeingDragged] = useState(null);
  const [squareBeingReplaced, setSquareBeingReplaced] = useState(null);
  const [scoreDisplay, setScoreDisplay] = useState(0);


  // check for columns of 4
  const checkForColumnOfFour = () => {

    // 39 is the index of the grid where we need to stop counting lines of 3 colors
    for (let i = 0; i <= 39; i++){
      const columnOfFour = [i, i + width, i + width * 2, i + width * 3];

      // grab the first color
      const decidedColor = currentColorArrangement[i];

      const isBlank = currentColorArrangement[i] === blank;

      if (columnOfFour.every(square => currentColorArrangement[square] === decidedColor && !isBlank)) {
        setScoreDisplay((score) => score + 4);
        columnOfFour.forEach(square => currentColorArrangement[square] = blank);
        return true;
    }
    }
  }

  // check for columns of 3
  const checkForRowOfFour = () => {
    // check all 64 squares
    for (let i = 0; i < 64; i++){
      const rowOfFour = [i, i + 1, i + 2, i + 3];

      // grab the first color
      const decidedColor = currentColorArrangement[i];

      // squares that I don't want to check, cause it's considered a row of 2 and are inside of rows of 3
      const notValid = [5, 6, 7, 13, 14, 15, 21, 22, 23, 29, 30, 31, 37, 38, 39, 45, 46, 47, 53, 54, 55, 62, 63, 64];
      const isBlank = currentColorArrangement[i] === blank;

      // if the notValid includes the square it's on the loop, than just continue and don't check those squares
      if(notValid.includes(i)) continue;

      if (rowOfFour.every(square => currentColorArrangement[square] === decidedColor && !isBlank)) {
        setScoreDisplay((score) => score + 4);
        rowOfFour.forEach(square => currentColorArrangement[square] = blank);
        return true;
    }
    }
  }

  // check for columns of 3
  const checkForColumnOfThree = () => {

    // 47 is the index of the grid where we need to stop counting lines of 3 colors
    for (let i = 0; i <= 47; i++){

      // this is pratically -> [0, 0 + 8 , 0 + (8*2)] -> goes horizontally
      const columnOfThree = [i, i + width, i + width * 2];

      // grab the first color
      const decidedColor = currentColorArrangement[i];
      const isBlank = currentColorArrangement[i] === blank;

      if (columnOfThree.every(square => currentColorArrangement[square] === decidedColor && !isBlank)) {
        setScoreDisplay((score) => score + 3)
        columnOfThree.forEach(square => currentColorArrangement[square] = blank)
        return true
    }
    }
  }

  // check for columns of 3
  const checkForRowOfThree = () => {
    // check all 64 squares
    for (let i = 0; i < 64; i++){
      const rowOfThree = [i, i + 1, i + 2];

      // grab the first color
      const decidedColor = currentColorArrangement[i];

      // squares that I don't want to check, cause it's considered a row of 2 and are inside of rows of 3
      const notValid = [6, 7, 14, 15, 22, 23, 30, 31, 38, 39, 46, 47, 54, 55, 63, 64];
            const isBlank = currentColorArrangement[i] === blank;

      // if the notValid includes the square it's on the loop, than just continue and don't check those squares
      if(notValid.includes(i)) continue;

      if (rowOfThree.every(square => currentColorArrangement[square] === decidedColor && !isBlank)) {
        setScoreDisplay((score) => score + 3);
        rowOfThree.forEach(square => currentColorArrangement[square] = blank);
        return true;
    }
    }
  }


  //move the squares bellow
  const moveIntoSquareBelow = () => {
    for(let i = 0; i <= 55; i++){

      // indexes of first row
      const firstRow = [0, 1, 2, 3, 4, 5, 6, 7];
      const isFirstRow = firstRow.includes(i);

      if (isFirstRow && currentColorArrangement[i] === blank) {
        let randomNumber = Math.floor(Math.random() * candyColors.length);
        currentColorArrangement[i] = candyColors[randomNumber];
    }

      // if the square below the one I'm looking at the moment + the width equals to nothing (blank)
      if((currentColorArrangement[i + width]) === blank){
        // moving the square I'm looping down
        currentColorArrangement[i + width] = currentColorArrangement[i];

        // so the one that was above it's now blank
        currentColorArrangement[i] = blank;
      }
    }
  }


  const dragStart = (e) => {
    setSquareBeingDragged(e.target);
  }
  const dragDrop = (e) => {
    setSquareBeingReplaced(e.target);
  }
  const dragEnd = () => {
    // to get a number instead of a string
    const squareBeingDraggedId = parseInt(squareBeingDragged.getAttribute('data-id'));
    const squareBeingReplacedId = parseInt(squareBeingReplaced.getAttribute('data-id'));

    currentColorArrangement[squareBeingReplacedId] = squareBeingDragged.getAttribute('src');
    currentColorArrangement[squareBeingDraggedId] = squareBeingReplaced.getAttribute('src');

    const validMoves = [
        squareBeingDraggedId - 1,
        squareBeingDraggedId - width,
        squareBeingDraggedId + 1,
        squareBeingDraggedId + width
    ];

    const validMove = validMoves.includes(squareBeingReplacedId);

    const isAColumnOfFour = checkForColumnOfFour();
    const isARowOfFour = checkForRowOfFour();
    const isAColumnOfThree = checkForColumnOfThree();
    const isARowOfThree = checkForRowOfThree();

    if (squareBeingReplacedId &&
        validMove &&
        (isARowOfThree || isARowOfFour || isAColumnOfFour || isAColumnOfThree)) {
        setSquareBeingDragged(null);
        setSquareBeingReplaced(null);
    } else {
        currentColorArrangement[squareBeingReplacedId] = squareBeingReplaced.getAttribute('src');
        currentColorArrangement[squareBeingDraggedId] = squareBeingDragged.getAttribute('src');
        setCurrentColorArrangement([...currentColorArrangement]);
    }
  }



  // create an array with 64 items, using the array candyColors
  const createBoard = () => {
    
    const randomColorArrangement = [];

    // width * width gives me the 64 (random colors) that I need in my board
    for (let i = 0; i < width * width; i++){
      // Math.floor to make sure that I get full numbers and candyColors.length of the array to spin a number between 0 and 5 
      const randomNumberFrom0to5 = Math.floor(Math.random() * candyColors.length);
      
      // get a random color
      const randomColor = candyColors[randomNumberFrom0to5];

      // push the random color into the randomColorArrangement array
      randomColorArrangement.push(randomColor);
    }
    setCurrentColorArrangement(randomColorArrangement);
  }

  useEffect(() => {
    createBoard();

    // leave the empty array cause I only want it to pass once
  }, []);
  
  // checkForColumnOfThree
  useEffect( () => {

    // check columns of 3 every 100 milisecond, because the board will change a lot, so I create a timer that will check my board
    const timer = setInterval(() => {
      // I want this to check for columns of 3 and 4 every 1000 seg, so I've created a timer here that will check my board every 1000 seg
      checkForColumnOfFour();
      checkForRowOfFour();
      checkForColumnOfThree();
      checkForRowOfThree();

      moveIntoSquareBelow();

      // if there is columns of 3 I'm going to reset the current color arrangement
      setCurrentColorArrangement([...currentColorArrangement]);
    }, 100);

    // and each time this runs I'm going to restart this
    return () => clearInterval(timer);

  }, [checkForColumnOfFour, checkForRowOfFour, checkForColumnOfThree, checkForRowOfThree, moveIntoSquareBelow, currentColorArrangement]);


  return (
    <div className="app">
      <div className="game">
        {currentColorArrangement.map((candyColor, index) => (
          <img
            key={index}
            src={candyColor}
            alt={candyColor}
            data-id={index}
            draggable={true}
            onDragStart={dragStart}
            onDragOver={(e) => e.preventDefault()}
            onDragEnter={(e) => e.preventDefault()}
            onDragLeave={(e) => e.preventDefault()}
            onDrop={dragDrop}
            onDragEnd={dragEnd}
          />
        ))}
      </div>
      <div className="candyText">
        <h1>Candy Crush Clone</h1>
        <ScoreBoard score={scoreDisplay}/>
      </div>
      
    </div>
  );
}

export default App;
